
using NUnit.Framework;
using TRPO_Lab3.Lib;
using System;

namespace TRPO_Lab3.Tests
{
    public class Tests
    {
        [Test]
        public void TestCalc()
        {
            const double widthTop = 5;
            const double widthBot = 7;
            const double height = 10;
            double rigth = 60;
            double result = TropSquare.FindTropSquare(widthTop, widthBot, height);
            Assert.AreEqual(rigth, result, "������� ��������� = {0}", rigth);
         
        }
        [Test]
        public void TestExeptions()
        {
            const double widthTop = -15;
            const double widthBot = 0;
            const double height = -0.1;

            Assert.Throws<ArgumentException>(()=>TropSquare.FindTropSquare(widthTop, widthBot, height));
            
        }
    }
}