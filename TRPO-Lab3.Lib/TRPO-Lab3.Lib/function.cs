﻿using System;

namespace TRPO_Lab3.Lib
{
    public class TropSquare
    {
        public static double FindTropSquare(double? widthTop, double? widthBot, double? height)
        { 
            if(widthBot <=0 || widthBot == null)
                throw new ArgumentException(nameof(widthBot));
            if (widthTop <= 0 || widthTop == null)
                throw new ArgumentException(nameof(widthTop));
            if (height <= 0 || height == null)
                throw new ArgumentException(nameof(height));
            return Convert.ToDouble(((widthBot + widthTop) / 2) * height); 
        }
    }
}
