﻿using System;
using System.Collections.Generic;
using TRPO_Lab3.Lib;

namespace TRPO_Lab3.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Сейчас мы попробуем посчитать площадь тропеции");
            List<double> param = new List<double>();
            start:
            try
            {
                while (param.Count != 3)
                {
                    Console.WriteLine($"Введите {param.Count + 1} параметр");
                    double p = Convert.ToDouble(Console.ReadLine());
                    if (p <= 0) continue;
                    param.Add(p);
                }
                Console.WriteLine("Результат: " + TropSquare.FindTropSquare(param[0], param[1], param[2]));
            }
            catch (Exception)
            { Console.WriteLine("некорректный параметр, давай по новой");
                goto start; }
                Console.ReadKey();
        }
    }
}
